package core;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import validator.ColorUndefinedException;

@AllArgsConstructor
@NoArgsConstructor
public enum ColorType {
    RED(255,0,0),
    BLACK(255,255,255),
    NONE {
        @Override
        public Integer getB() {
            throw new ColorUndefinedException();
        }

        @Override
        public Integer getG() {
            throw new ColorUndefinedException();
        }

        @Override
        public Integer getR() {
            throw new ColorUndefinedException();
        }
    };

    @Getter
    Integer r, g, b;
}
