package core;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Car {
    private ColorType color;
    private BigDecimal price;
    private CarType type;

    public static CarBuilder builder() {
        return new CarBuilder();
    }

    public static class CarBuilder {
        private ColorType color;
        private BigDecimal price;
        private CarType type;

        public CarBuilder type(CarType type) {
            this.type = type;
            return this;
        }

        public CarBuilder color(ColorType color) {
            this.color = color;
            return this;
        }

        public CarBuilder price(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Car build() {
            return new Car(color, price, type);
        }
    }
}
