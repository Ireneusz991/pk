package validator;

public class ColorUndefinedException extends RuntimeException {

    public ColorUndefinedException() {
        super("Color undefined !!!");
    }
}
