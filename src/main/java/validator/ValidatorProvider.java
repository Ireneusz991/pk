package validator;

import java.util.List;

class ValidatorProvider {
    private static List<CarValidator> validators;

    static {
        validators = List.of(new CarColorValidatorImpl(), new CarTypeValidatorImpl());
    }

    static List<CarValidator> getValidators() {
        return validators;
    }
}
