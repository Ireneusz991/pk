package validator;

import core.Car;
import stereotype.Validator;

public interface CarValidator extends Validator<Car> {
}
