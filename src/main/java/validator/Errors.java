package validator;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;

public class Errors extends ArrayList<ErrorDTO> {

    public void throwIfHasErrors() {
        if (CollectionUtils.isNotEmpty(this)) {
            throw new ValidationException(this);
        }
    }
}
