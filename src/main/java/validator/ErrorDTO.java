package validator;

import lombok.Data;
import lombok.NonNull;

@Data
public class ErrorDTO {
    @NonNull
    private String value;

    @Override
    public String toString() {
        return "error:" + value;
    }
}
