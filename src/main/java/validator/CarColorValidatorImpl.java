package validator;

import core.Car;
import core.ColorType;

import java.util.Objects;

class CarColorValidatorImpl implements CarValidator {

    public ErrorDTO validate(Car car) {
        if(car.getColor() != ColorType.BLACK) {
            return new ErrorDTO("not my color");
        }

        return null;
    }

    public boolean isAcceptable(Car car) {
        return Objects.nonNull(car.getColor());
    }
}
