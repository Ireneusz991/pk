package validator;

import core.Car;
import core.CarType;

import java.util.Objects;

class CarTypeValidatorImpl implements CarValidator {

    public ErrorDTO validate(Car car) {
        if (CarType.SUV != car.getType()) {
            return new ErrorDTO("Wrong car type");
        }

        return null;
    }

    public boolean isAcceptable(Car car) {
        return Objects.nonNull(car.getType());
    }
}
