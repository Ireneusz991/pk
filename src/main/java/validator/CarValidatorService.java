package validator;

import core.Car;

import java.util.List;
import java.util.Objects;

public final class CarValidatorService {

    public static void validate(Car car) {
        List<CarValidator> validators = ValidatorProvider.getValidators();

        Errors errors = new Errors();
        validators
                .stream()
                .filter(f -> f.isAcceptable(car))
                .map(v -> v.validate(car))
                .filter(Objects::nonNull)
                .forEach(e -> errors.add(e));

        errors.throwIfHasErrors();
    }
}
