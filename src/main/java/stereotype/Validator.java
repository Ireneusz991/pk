package stereotype;

import validator.ErrorDTO;

public interface Validator<C> {
    ErrorDTO validate(C obj);
    boolean isAcceptable(C obj);
}
