package excel;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Main {

    public static void main(String[] args) throws IOException, InvalidFormatException {
        File file = new File("pathname");
        InputStream is = new FileInputStream(file);

        OwnSheet ownSheet = ExcelReader.read(is);
        System.out.println(ownSheet);
    }
}
