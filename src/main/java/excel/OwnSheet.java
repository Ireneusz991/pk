package excel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@ToString
public class OwnSheet {
    List<OwnRow> rows = new ArrayList<>();
}
