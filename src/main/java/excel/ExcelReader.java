package excel;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.IOException;
import java.io.InputStream;

import static org.apache.poi.ss.usermodel.Row.MissingCellPolicy.CREATE_NULL_AS_BLANK;

public class ExcelReader {

   public static OwnSheet read(InputStream inputStream) throws IOException, InvalidFormatException {
        Workbook sheets = WorkbookFactory.create(inputStream);

        Sheet sheet = sheets.getSheetAt(0);

        OwnSheet ownSheet = new OwnSheet();
        for (Row row : sheet) {
            OwnRow ownRow = new OwnRow();
            ownSheet.getRows().add(ownRow);

            Cell cell1 = row.getCell(0, CREATE_NULL_AS_BLANK);
            Cell cell2 = row.getCell(1, CREATE_NULL_AS_BLANK);
            Cell cell3 = row.getCell(2, CREATE_NULL_AS_BLANK);
            Cell cell4 = row.getCell(3, CREATE_NULL_AS_BLANK);

            String value1 = getValue(cell1);
            String value2 = getValue(cell2);
            String value3 = getValue(cell3);
            String value4 = getValue(cell4);
            ownRow.setVal1(value1);
            ownRow.setVal2(value2);
            ownRow.setVal3(value3);
            ownRow.setVal4(value4);
        }

        return ownSheet;
    }
    private static String getValue(Cell cell) {
        CellType cellTypeEnum = cell.getCellTypeEnum();

        switch (cellTypeEnum) {
            case NUMERIC:
                return Double.toString(cell.getNumericCellValue());
            case BLANK:
                return "";
            case BOOLEAN:
                return cell.getBooleanCellValue() + "";
            default:
                return cell.getStringCellValue();
        }
    }
}
