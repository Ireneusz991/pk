import core.Car;
import core.CarType;
import core.ColorType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import validator.CarValidatorService;
import validator.ValidationException;

import java.math.BigDecimal;

public class CarValidatorTest {

    @Test
    public void failIfNotBlackAndSuv() {
        Car.CarBuilder builder = Car.builder()
                .color(ColorType.RED)
                .price(BigDecimal.ZERO)
                .type(CarType.S);

        Car car = builder.build();

        Assertions.assertThrows(ValidationException.class, () -> {
            CarValidatorService.validate(car);
        });
    }

    @Test
    public void suvAndBlackTest() {
        Car.CarBuilder builder = Car.builder()
                .color(ColorType.BLACK)
                .price(BigDecimal.ZERO)
                .type(CarType.SUV);

        Car car = builder.build();

        CarValidatorService.validate(car);

        Assertions.assertEquals(ColorType.BLACK, car.getColor());
        Assertions.assertEquals(CarType.SUV, car.getType());
    }
}
