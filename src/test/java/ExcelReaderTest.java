import excel.ExcelReader;
import excel.OwnRow;
import excel.OwnSheet;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;

public class ExcelReaderTest {

    @Test
    public void expect4Rows() throws IOException, InvalidFormatException {
        File file = new File("C:\\Users\\wojsam\\Documents\\JAVA TEST\\TEST.xlsx");
        InputStream is = new FileInputStream(file);

        OwnSheet ownSheet = ExcelReader.read(is);

        Assertions.assertEquals(4, ownSheet.getRows().size(),"Expect 4 rows");
    }


    @Test
    public void expectFirstRowVal2NotNull() throws IOException, InvalidFormatException {
        File file = new File("C:\\Users\\wojsam\\Documents\\JAVA TEST\\TEST.xlsx");
        InputStream is = new FileInputStream(file);

        OwnSheet ownSheet = ExcelReader.read(is);
        OwnRow ownRow = ownSheet.getRows().get(0);

        Assertions.assertNotNull(ownRow.getVal2(),"Value 1 cannot be null");
    }

    @Test
    public void expectFirstRowVal1IsNull() throws IOException, InvalidFormatException {
        File file = new File("C:\\Users\\wojsam\\Documents\\JAVA TEST\\TEST.xlsx");
        InputStream is = new FileInputStream(file);

        OwnSheet ownSheet = ExcelReader.read(is);
        OwnRow ownRow = ownSheet.getRows().get(0);

        Assertions.assertNull(StringUtils.trimToNull(ownRow.getVal1()),"Value 1 must be null");
    }
}
